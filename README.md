# R avancé : le nécessaire pour créer un package R (et faire du code réutilisable) #

## Introduction
Utiliser des package R est la meilleure manière de coder en R de manière réutilisable. Et c'est très facile à apprendre. Vraiment. 
### Qu'est-ce que ce cours ? ###
Il permet d'apprendre tout ce qui est nécessaire pour utiliser de manière optimale les package R :

* Créer un package avec devtools
* Ecrire et gérer des tests avec testthat
* Ecrire et gérer la documentation avec roxygen

### Qu'est-ce qui est fourni ? ###
* Ce document : un intermédiaire entre notes de cours et "cheat sheet"
* Un exemple de package "Hello World!": le répertoire "monNouveauPackage/"

### Pré-requis ###
Programmation de base en R: 

* Savoir installer/charger un package dans R (install.packages()/load())
* savoir manipuler des variables classiques dans R
* Savoir utiliser l'aide de R
* Savoir faire des fonctions dans R

Si vous cherchez à acquérir ce type de compétence vous pouvez essayer [cet autre cours](https://bitbucket.org/cbarbu/basicrcourse).
 
### Comment contribuer ? ###
Ce cours est libre de droits, vous pouvez l'utiliser comme bon vous semble. Les contributions sont aussi bienvenues: 

* Corrections d'erreurs grammaticales/fautes d'orthographe
* Demandes de clarifications 
* Formattage markdown ou knitR
* "Trucs" importants que j'aurais manqué. 

Si vous souhaitez apporter ce genre de contributions, faites un fork et soumettez des pull request. Ou si vous ne maîtrisez pas git ajouter une "issue". 

## Corps du cours ##

### Se préparer ###
Les outils clés (packages R) pour la création d’un package : 
*  devtools
*  roxygen
*  testthat

### Création d’un package avec devtools ###
Dans R

```
#!R
library("devtools")
create("monNouveauPackage")
```

Ca permet d'avoir un répertoire de base

```
#!R
setwd("monNouveauPackage")
```
puis se mettre dans le répertoire nouvellement créé

```
#!R
document()
```

pour charger/mettre à jour en mémoire le code écrit

### La base des tests
Pour lancer les tests il suffit de faire: 

```
#!r

test()
```

pas contents mais si on lui dit 1 (yes) nous créé toute l'infrastructure pour les tests: 
un répertoire et notamment tests/testthat/ , le répertoire où on doit mettre les tests
il ne manque plus qu'à créer un fichier tests/testthat/test_mesFonctions.R
pour que test() lance ce fichier testant les fonctions dans R/mesFonctions.R

Les tests sont organisés hierarchiquements: 
* en fichiers testant les fichiers de fonctions correspondants
* en groupes test_that("titre de ce que je veux tester",{ tous les tests que je veux appliquer })
* tests sous la forme expect_[true|output|whatever comes up when doing tab on "expect_" quand on a chargé testthat]

NB: à condition d'avoir fait document() (juste avant), on peut copier coller tout ce qui 
voir les exemples

### Le debuggage
If everything you wrote is without bugs at the first trial, you probably did't do enough tests. 
Go back to the former paragraph. 

If you do not pass your tests and you don't understand why, you need to debug your code. 
Here are the key tricks: 

0) when faced with an error, traceback() allows you to see what was the chain of functions call leading to the error

1) debug(myFunction) , on the command line
Makes the function stop at its beginning the next time it is called. 
This allows to display the value of variables and execute the code step by step.
Note that sourcing again your function will restore the normal behavior. You can also do
undebug(myFunction)
When debugging you can use the following special commands (see browser fn help)
* Enter: execute the next line
* c : continue until getting out of the function or of the loop
* s : get into the next function call
* Q : stops everything

2) browser(myFunction) , in the definition of the function
Makes the function stop, just like debug() but precisely where you stick it in the function. 
Specially helpful is the "expr" parameter allowing to stop only if a given expression is evaluated to TRUE
You can then stop only if a variable has a given value.

3) recover(), called interactively when using browser() or debug(), allows to see the chain of calls
up to where you are. In addition it allows to switch to one of the environments in this calling chain, allowing
you to check whatever variable you want.
A very common use of recover is as a value for the "error" option:

```
#!r
options(error=recover)
```

Then whenever an error happen recover is called allowing you to do post-mortem debugging. 

In addition you might find helpfull to set the following options (see function options) you can put them in your .Rprofile or simply enter them in the command line when needed: 

```
#!r
options(showErrorCalls = TRUE) # force display of the call chain when an error is issued
options(showWarnCalls = TRUE)  # force display of the call chain when a warning is issued
```

### Les données incluses dans le package
créer la variable contenant les données (n'importe quel type) et l'enregistrer :

```
#!r
save(maVariable,file="maVariable.rda")
```

puis le mettre dans data/
si dans DESCRIPTION LazyData est "true" toutes les données dans data/ seront chargées par document()
ou par library("monNouveauPackage") si LazyData: false, il faudra faire data(maVariable)

### installation du package
En local, il suffit de charger devtools dans le répertoire du package et : 

```
#!r
check() # pour vérifier que tout va bien, notamment les tests et les exemples
install() # pour installer
```

si on veut qu'il soit sur CRAN il faut être un peu plus motivé

### La documentation 
On utilise "roxygen2" qui s'intégre naturellement dans "devtools", simplement en faisant le document() 
que l'on a déjà vu

Les commentaires interprétés par roxygen2 sont les lignes commencées par "#'" 

Pour pouvoir écrire en français et donc utiliser des accents (caractères non ASCII) il 
suffit d'ajouter dans le fichier DESCRIPTION une ligne indiquant l'encodage des fichiers
sous linux ou mac, c'est probablement UTF-8, vous ajouterez donc
Encoding: UTF-8

### Exportation
Par défaut, create() exporte toutes les fonctions et données respectivement dans R/ et data/
ce n'est pas une très bonne idée vu que l'on ne veut pas toujours que les petits utilitaires soient publiques
donc on supprime le fichier NAMESPACE généré automatiquement, document() permet de le recréer mais géré maintenant
par roxygen. Ensuite il ne faut jamais éditer manuellement NAMESPACE.

pour exporter une fonction, il suffit d'ajouter :

```
#!r
#' @export nomDeMaFonction
```

à la fin du commentaire roxygen de ma fonction

Pour les données, il est important aussi de les documenter et exporter (on peut les documenter 
dans n'importe quel fichier du répertoire R/ , avoir un fichier spécialisé dans la documentation des données 
est sans doute une bonne idée.

### Package avec du code c

Pour une introduction à l'écriture de code c en R voir l'excellente "Calling C code from R, an introduction"
par Signal Blay : http://www.sfu.ca/~sblay/ (Presentations)

Dans la manière du possible, utiliser Rcpp en ligne, si ce n'est pas possible (code faisant appel à des librairies C),
les fichiers de code C doivent être mis dans /src/.
Mais on peut toujours utilser Rcpp. 

#### Code C de base
Pour faire du code c de base, suivre le "Hello world" en c:

src/fntest.c 

et le code R l'appellant:

R/fntest.R

Ces fichiers seront automatiquement compilés en un fichier nomDeMonPackage.so par 
document() 
pour pouvoir utiliser les fonctions définies en C il suffit d'ajouter:

```
#!r
#' @useDynLib nomDeMonPackage
```

dans le paragraphe documentant la fonction R

Par ailleurs, c'est une bonne chose, dans l'appel de la fonction (.C par exemple) d'indiquer où chercher
la fonction c, par exemple: 
```
#!r
.C("FnTest",PACKAGE=nomDeMonPackage)
```

NB: dans le fichier exemple on utilise un double pointeur pour la chaine de charactères: une chaîne de 
    charactères est un pointeur, et l'interface avec R impose un pointeur pour chaque argument.
NB2: Toute sortie doit se faire via les arguments de la fonction, et il faut qu'ils aient la bonne taille.
    Pour des chaines de charactères il faut être particulièrement vigilant (voir exemple). 
    De manière plus générale, l'interface .Call ou l'utilisation de Rcpp permet d'avoir beaucoup moins 
    de problèmes de tailles de mémoire.

### L'optimisation du code
L'optimisation du code c'est comment le faire aller plus vite. Le sujet est très bien couvert par ailleurs. 
Vous devez au moins lire les citations de Donald Knuth dans l'excellent cours d'Hadley Wickham:

http://adv-r.had.co.nz/Profiling.html

Pour regarder le temps que prends un bout de code: system.time(), ou proc.time().

Par ailleurs, pour un exemple en pratique, j'ai écrit une réponse à une question sur Stack Overflow qui décrit pas à pas les étapes à suivre sur un exemple concret: http://stackoverflow.com/a/28674397/1174052

Si vous suivez ça, vous saurez optimizer. Le code de départ est attaché: optimization/help_ref.R, ainsi que le code final (temps d'excécution divisé par 30): optimization/help.R

### Où mettre son bazar:
Tout ce qui ne rentre pas dans les répertoires précédemment cités peut aller 
dans inst/
Dans le passé et encore dans pas mal de vieux packages c'est là que sont les tests, c'est aussi là que sont les vignettes (les manuels de présentation de packages).
