/*
 * =====================================================================================
 *
 *       Filename:  fntest.c
 *
 *    Description:  exemple of Hello world fonction en C, note 
 *
 *        Version:  1.0
 *        Created:  18/02/2015 13:29:37
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  C. Barbu (CB), corentin.barbu@gmail.com
 *        Company:  INRA, France
 *
 * =====================================================================================
 */

#include <stdio.h>

void HelloWorldInC (char **name,double * value,char **out, int *nCharOut){
    char message[*nCharOut];
    sprintf(message,"Hello world, specially %s number %f\n",*name,*value);
    printf("%s",message);
    memcpy(*out,&message[0],*nCharOut-1);
    *value +=1;
}

