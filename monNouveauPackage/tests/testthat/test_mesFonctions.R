library("testthat")
# fichier permettant de tester les fonctions dans R/mesFonctions.R
test_that("HelloWorld() works fine",{
          # test entrée générant une erreur
          entreeBof <- "quelquechose"
          expect_error(HelloWorld(entreePasOK))

          # test entrée générant le warning
          entreePasOK<- "something"
          expect_warning(HelloWorld(entreeBof),"entree")
          suppressWarnings(expect_equal(HelloWorld(entreeBof),2))

          # test entrée chaine de charactere vide
          entreeOK <- ""
          expect_equal(HelloWorld(entreeOK),1)
          # test si pas d'entrée
          expect_output(a<-HelloWorld(),"Hello World !")
          expect_equal(a,0)
})

